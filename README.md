# vue-template

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

Place .env file root directory. For example:
```
SERVER_URL='"https://db2-dev.leadza.ai/parse"'
STRIPE_APP_ID='"pk_test_VShqmlSC2qozYzj3qFxwzLfr"'
APP_ID='"f657bb1d-d507-4440-9b2e-385e5fe6f300"'
SENTRY_DSN='""'
FB_APP_ID='"1835942169980580"'
```
