'use strict'
const path = require('path')
const config = require('../config')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const packageConfig = require('../package.json')

exports.assetsPath = function (_path) {
  const assetsSubDirectory = process.env.NODE_ENV === 'production'
    ? config.build.assetsSubDirectory
    : config.dev.assetsSubDirectory

  return path.posix.join(assetsSubDirectory, _path)
}

exports.cssLoaders = function (options = {}) {
  const cssLoader = {
    loader: 'css-loader',
    options: {
      sourceMap: options.sourceMap,
      modules: true,
      localIdentName: '[name]__[local]_[hash:base64:5]'
    }
  }

  const postcssLoader = {
    loader: 'postcss-loader',
    options: {
      sourceMap: options.sourceMap
    }
  }

  // generate loader string to be used with extract text plugin
  function generateLoaders (loader, loaderOptions) {
    const loaders =
        options.extract ? MiniCssExtractPlugin.loader : 'vue-style-loader';
    const result = [
      {
        resourceQuery: /module/,
        use: [
          loaders,
          {
            loader: 'css-loader',
            options: {
              sourceMap: options.sourceMap,
              modules: true,
              localIdentName: '[name]__[local]_[hash:base64:5]'
            }
          },
          postcssLoader,
        ]
      },
      {
        use: [
          loaders,
          {
            loader: 'css-loader',
            options: {
              sourceMap: options.sourceMap,
              modules: false
            }
          },
          postcssLoader,
        ]
      }
    ];

    if (loader) {
      const loaderObject = {
        loader: loader + '-loader',
        options: Object.assign({}, loaderOptions, {
          sourceMap: options.sourceMap
        })
      };

      result[0].use.push(loaderObject);
      result[1].use.push(loaderObject);
    }

    // Extract CSS when that option is specified
    // (which is the case during production build)
    return result;
  }

  const globalImport = {
    import: [
      path.resolve(__dirname, '../src/styles/global.styl')
    ]
  };

  return {
    css: generateLoaders(),
    postcss: generateLoaders(),
    sass: generateLoaders('sass', { indentedSyntax: true }),
    scss: generateLoaders('sass'),
    stylus: generateLoaders('stylus', globalImport),
    styl: generateLoaders('stylus', globalImport)
  }
}

// Generate loaders for standalone style files (outside of .vue)
exports.styleLoaders = function (options) {
  const loaders = exports.cssLoaders(options)

  return Object.keys(loaders).map((extension) => ({
      test: new RegExp('\\.' + extension + '$'),
      oneOf: loaders[extension]
    }))
}

exports.createNotifierCallback = () => {
  const notifier = require('node-notifier')

  return (severity, errors) => {
    if (severity !== 'error') return

    const error = errors[0]
    const filename = error.file && error.file.split('!').pop()

    notifier.notify({
      title: packageConfig.name,
      message: severity + ': ' + error.name,
      subtitle: filename || '',
      icon: path.join(__dirname, 'logo.png')
    })
  }
}
