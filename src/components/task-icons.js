import CheckedIcon from '@svg/checked.svg?inline';
import NotificationIcon from '@svg/notification.svg?inline';
export default {
    NotificationIcon,
    CheckedIcon
};
