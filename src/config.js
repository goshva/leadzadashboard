import Vue from 'vue';
import Parse from 'parse';
import Raven from 'raven-js';
import RavenVue from 'raven-js/plugins/vue';

/* eslint-disable no-undef, no-process-env */
const { APP_ID, SERVER_URL, SENTRY_DSN, NODE_ENV } = process.env;

Vue.config.productionTip = NODE_ENV === 'production';

Parse.initialize(APP_ID);
Parse.serverURL = SERVER_URL;

Raven
    .config(SENTRY_DSN, { environment: NODE_ENV })
    .addPlugin(RavenVue, Vue)
    .install();
