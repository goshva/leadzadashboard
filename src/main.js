import Vue from 'vue';
import Root from './Root';
import './config';
import store from './store';
import router from './router';
import { installBus } from './utils/event-bus';
import filters from './utils/filters';
import fbSdk from './utils/fb-sdk';
import VueYoutube from 'vue-youtube';
Object.keys(filters).forEach(name => {
    Vue.filter(name, filters[name]);
});

Vue.use(installBus);
Vue.use(fbSdk);
Vue.use(VueYoutube)
//window.Event = new Vue();
/* eslint-disable no-new */
new Vue({
    router,
    store,
    render: h => h(Root)
}).$mount('#root');
