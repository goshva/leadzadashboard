import store from '@/store';  
import { isCookieEnabled, getCookie } from 'tiny-cookie';
function loadPage (page) {
    return () => import(/* webpackChunkName: "[request]" */ `@/pages/${page}`);
}

export default [
    {
        path: '/',
        name: 'home',
        component: loadPage('Home'),
        beforeEnter (to, from, next) {
            if (!! getCookie('apiToken')){
                const token  = getCookie('apiToken');
                const id  = getCookie('fbid');
                const user = {
                    id: id,
                    loggedIn: true,
                    access_token: token
                };
                store.dispatch('auth/LogIn',user);
                //if ( getCookie('sawvideo') !== null){
                //    next('/getstartedvideo');
                // }
                next();
            } else {
                next('/login');
            }
        },
        children: [
            {
                path: '/optimization',
                name: 'optimization',
                components: {
                    modal: loadPage('Optimization')
                }
            },
            {
                path: '/plans',
                name: 'plans',
                components: {
                    modal: loadPage('Plans')
                }
            },
            {
                path: '/billing',
                name: 'billing',
                components: {
                    modal: loadPage('Billing')
                }
            },
            {
                path: '/settings',
                name: 'settings',
                components: {
                    modal: loadPage('Settings')
                }
            },
            {
                path: '/getstartedvideo',
                name: 'getstartedvideo',
                components: {
                    modal: loadPage('GetStartedVideo')
                }
            }
        ]
    },
    {
        path: '/login',
        name: 'login',
        component: loadPage('Login'),
    },
    {
        path: '/bot',
        name: 'bot',
        component: loadPage('Bot')
    },
    {
        path: '/old-browser',
        name: 'old-browser',
        component: loadPage('OldBrowser')
    },
    {
        path: '/404',
        name: '404',
        component: loadPage('404')
    },
    {
        path: '*',
        redirect: { name: '404' }
    }
];

