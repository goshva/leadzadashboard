import Vue from 'vue';
import Vuex from 'vuex';
import pricingModule from './modules/pricing';
import settingsModule from './modules/settings';
import auth from './modules/auth';
import apiSettings from './modules/apiSettings';

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        pricingModule,
        settingsModule,
        auth,
        apiSettings
    },
    strict: !Vue.config.productionTip
});

export default store;
