/* eslint-disable no-shadow */
const getters = {
    getAdById: state => id => state.ads.find(ad => ad.id === id),
    getSortedAds: (state, getters) => {
        const last_month_spend_usd = id => getters.getCampaginTotalSpend(id);
        return [...state.ads].sort((a, b) => last_month_spend_usd(b.id) - last_month_spend_usd(a.id));
    },

    getAccountById: (_, getters) => (adId, index) => {
        return getters.getAdById(adId).campaigns[index];
    },

    isAllAccountsChecked: (_, { getAdById }) => id => {
        return getAdById(id).campaigns.every(account => account.enabled);
    },
    isAnyCampaignChecked: (_, { getAdById }) => id => {
        return getAdById(id).campaigns.some(account => account.enabled);
    },

    isAccountChecked: (_, { getAdById }) => id => {
        return getAdById(id)["enabled"];
//      return isAnyCampaignChecked(id);
    },

    getCampaginTotalSpend: (_, { getAdById }) => id => {
        return getAdById(id).campaigns.reduce((total, { last_month_spend_usd }) => total + last_month_spend_usd, 0);
    },
 
    getCampaginTotalCheckedSpend: (_, { getAdById }) => id => {
        return getAdById(id).campaigns.reduce((total, { enabled, last_month_spend_usd }) => total + last_month_spend_usd* enabled, 0);
    },

    getTotalSpend: state => state.ads.reduce((total, { campaigns }) => {
        return total + campaigns.reduce((accTotal, { last_month_spend_usd }) => accTotal + last_month_spend_usd, 0);
    }, 0),

    getSelectedAccountsCount: state => state.ads.reduce((count, { campaigns }) => {
        return count + campaigns.filter(account => account.enabled).length;
    }, 0),

    getAccountsCount: state => state.ads.reduce((count, { campaigns }) => count + campaigns.length, 0),

    getCurrentSpend: state => state.ads.reduce((totalSpend, { campaigns }) => {
        return totalSpend + campaigns.reduce((accSpend, { enabled, last_month_spend_usd }) => {
            return accSpend + (enabled ? last_month_spend_usd : 0);
        }, 0);
    }, 0),
    isRateAvaliable: (_, getters) => rate => ((rate.checkSpend(getters.getCurrentSpend) && rate.limit(getters.getSelectedAccountsCount))),
    isOverLimit: (_, getters) => rate => rate.limit(getters.getSelectedAccountsCount),

    isCurrentRateAvaliable: ({ selectedRate }, getters) => {
        return selectedRate && getters.isRateAvaliable(selectedRate);
    },

    firstAvaliableRate: (state, { isRateAvaliable }) => state.rates.find(isRateAvaliable)
};
/* eslint-enable no-shadow */

export default getters;
