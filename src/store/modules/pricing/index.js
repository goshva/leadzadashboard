import state from './state';
import getters from './getters';

/* eslint-disable no-shadow */
const actions = {

    showAds ({ commit }, ads) {
        commit('ADDADS', ads);
    },
    checkParentAcc ({ getters, commit }, { index, value }) {
        const ad = getters.getAdById(index);
        commit('CHECK_PARENT_ACCOUNT', { ad, value });
    },
    checkAcc ({ getters, commit }, { index, value }) {
        const ad = getters.getAdById(index);
        commit('CHECK_ACCOUNT', { ad, value });
    }
};

const mutations = {
    CHECK_CAMPAIGN (_, { value, campaign }) {
        campaign.enabled = value;
    },
    ADDADS (state, ads) {
        state.ads = ads;
    },
    CHECK_PARENT_ACCOUNT (_, { ad }) {
        ad.enabled = true;
    },
    CHECK_ACCOUNT (_, { ad, value }) {
        ad.enabled = value;
        for (const account of ad.campaigns) {
           account.enabled = value;
        }
    },
    SELECT_RATE (state, rate) {
        state.selectedRate = rate;
        console.log(state.selectedRate);
    }
};
/* eslint-enable no-shadow */

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
};
