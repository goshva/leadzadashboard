import { fetchLabels } from '@/services/settings';

/* eslint-disable no-shadow */
const state = {
    labels: {}
};

const actions = {
    async getLabels ({ commit }) {
        const res = await fetchLabels();

        const labels = res.reduce((acc, label) => {
            acc[label.get('key')] = label.attributes;
            return acc;
        }, {});

        commit('SET_LABELS', labels);
    }
};

const getters = {
    getLabel: state => key => state.labels[key],
    getLabelsLength: state => Object.keys(state.labels).length
};

const mutations = {
    SET_LABELS (state, labels) {
        state.labels = labels;
    }
};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
};
