import UAParser from 'ua-parser-js';

const ua = navigator.userAgent;
const parser = new UAParser(ua);

const browserThreshold = {
    chrome: 50,
    firefox: 40,
    safari: 9,
    ie: 11
};

const tests = {
    touch: () => ('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0),
    iOS: () => parser.getOS().name === 'iOS',
    isMac: () => parser.getOS().name === 'Mac OS',
    isMobile: () => parser.getDevice().type === 'mobile',
    phantomjs: () => ua.toLowerCase().indexOf('phantomjs') > 0,
    isOldBrowser () {
        const { name, version } = parser.getBrowser();

        if (this.phantomjs() || !name || !version) {
            return false;
        }

        const browser = browserThreshold[name.toLowerCase()];
        return browser && browser > parseInt(version, 10);
    },
    smallScreen: () => window.innerWidth < 760,
    passive () {
        let supportsPassive = false;
        /* eslint-disable */
        try {
            const opts = Object.defineProperty({}, 'passive', {
                get: () => {
                    supportsPassive = true;
                }
            });
            window.addEventListener('testPassive', null, opts);
            window.removeEventListener('testPassive', null, opts);
        } catch (e) {}
        /* eslint-enable */

        return supportsPassive ? { passive: true } : false;
    }
};

const support = Object.keys(tests).reduce((result, feature) => {
    result[feature] = tests[feature]();
    return result;
}, {});

export default support;
