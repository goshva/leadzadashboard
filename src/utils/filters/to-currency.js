const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 0
});

function toCurrency (value) {
    if (typeof value === 'undefined') {
        return '';
    }
    return formatter.format(value).replace(/\,/g, ' ');
}

export default toCurrency;
