import i18nDictionary from '@/utils/i18nDictionary';
import { getCookie } from 'tiny-cookie';
function i18n (key) {
    let lang = getCookie('lang') || window.navigator.userLanguage || window.navigator.language
    if (lang !== 'ru') { lang = 'en' }
    let value = key;
    try {
        value  = this.i18nDictionary[key][this.$store.getters['auth/iso']] || this.i18nDictionary[key][lang.split('-')[0]];
    } catch(err) {
        value = '';
        console.log(err);
    }
    return value
}
export default i18n
